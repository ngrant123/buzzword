import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by nathangrant
 * 109816757
 *
 */
public class Gameplay1 {



    Stage primary;
    Scene primaryScene;
    VBox menu;
    HBox timeR;
    HBox guessedLetters;
    VBox correctWords;
    VBox centergrid;
    ScrollPane scroll;
    VBox target;
    GameData data;
    LevelLayout levelL;



    VBox sideM;
    VBox titleContainer;
    LoginSucces loginscreen;
    LevelSelection level;
    VBox middleDiagram;
    Button button1;
    Button button2;
    Button button3;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button10;
    Button button11;
    Button button12;

    HBox region1;
    HBox region2;
    HBox region3;
    Stage loginStage;
    GridPane diagram;
    ScrollPane display;




    BorderPane layout;
    Button homeB;
    Button login;
    Button start;
    Button pause;
    appGUI gui;
    apptemplate app;

    public Gameplay1(Stage primaryStage) {
        this.primary= primaryStage;


        //gui=app.g\
        //  this.primary = primary;
    }

    public Gameplay1(){

    }


    public void start(Stage primaryStage,GameData data1){

        // primaryStage= new Stage();
        //  this.primary=primaryStage;
        //   primary= new Stage();
        layout=new BorderPane();
        data=data1;
        levelL= new LevelLayout();
        //layout.setPadding(new Insets(10,);

        loginStage= new Stage();

        loginStage.setMaxHeight(primaryStage.getMaxHeight());
        loginStage.setMaxWidth(primaryStage.getMaxWidth());
        loginStage.setMinHeight(800);
        loginStage.setMinWidth(primaryStage.getMinWidth());
        loginscreen= new LoginSucces(loginStage);

        initStyleT2();
        initStyleSide2();
        initStyleG2();
        initStyleL2();
       // initStyleB2();

        //reset(layout);
        primaryScene= new Scene(layout);
        primary.setScene(primaryScene);

        //  app.getGUI().setStage(primary);
        //   primary.show();
    }


    public void initStyleSide2(){
        initStyleSide(layout);
    }

    public void initStyleSide(BorderPane layout){

        sideM=new VBox(30);
        sideM.setMinWidth(70);
        sideM.setPadding(new Insets(10,40,0,0));

        timeR= new HBox();
        timeR.setStyle("-fx-border-radius:5");


        timeR.setMinWidth(50);
        timeR.setPadding(new Insets(10,10,10,10));


        //FAKE
        Label title= new Label("Time remaing:");

        title.setLayoutY(50);
        title.setLayoutX(50);

        title.setStyle("-fx-text-fill:red");
        title.setFont(Font.font(20));


        timeR.getChildren().add(title);
        timeR.setStyle("-fx-text-fill:white;-fx-background-color:black");
        //timeR.setPadding(new Insets(20,20,20,20));

        sideM.getChildren().add(timeR);


        guessedLetters= new HBox();
        //FAKE use arraylist

        Label title2= new Label("B U");
        title2.setStyle("-fx-text-fill:white");
        title2.setFont(Font.font(20));


        guessedLetters.getChildren().add(title2);
        guessedLetters.setStyle("-fx-text-fill:white;-fx-background-color:black");
        guessedLetters.setPadding(new Insets(20,20,20,20));

        sideM.getChildren().add(guessedLetters);

        correctWords= new VBox(20);

        display= new ScrollPane();
        display.setMaxHeight(170);
        //display.setContent();

        //create("Dog");
        Label title3= new Label("DOG");
        title3.setStyle("-fx-text-fill:black");
        title3.setFont(Font.font(20));

       // display.setContent(create("DOG"));

        correctWords.getChildren().add(title3);

        Label title4= new Label("JUICE");
        title4.setStyle("-fx-text-fill:black");
        title4.setFont(Font.font(20));

       correctWords.getChildren().add(title4);

        Label title10= new Label("JUICE");
        title10.setStyle("-fx-text-fill:black");
        title10.setFont(Font.font(20));

        correctWords.getChildren().add(title10);
        //display.setContent(title3);

        Label title5= new Label("Total: ");
        //title5.setMinWidth(150);
        title5.setStyle("-fx-text-fill:black");
        title5.setFont(Font.font(35));


       // title5.setStyle("-fx-background-color:black;-fx-background-color:white");
       // title5.setFont(Font.font(20));

        correctWords.getChildren().add(title5);

        display.setContent(correctWords);
        //correctWords.setStyle("-fx-background-color:white");

        //sideM.getChildren().add(correctWords);
        sideM.getChildren().add(display);

        target= new VBox(15);
        target.setStyle("-fx-text-fill:white;-fx-background-color:black");

        Label title6= new Label("Target");
        title6.setStyle("-fx-text-fill:white");
        title6.setFont(Font.font(20));

        target.getChildren().add(title6);

        Label title7= new Label("10");
        title7.setStyle("-fx-text-fill:white;-fx-background-color:black");
        title7.setFont(Font.font(20));

        target.getChildren().add(title7);

        sideM.getChildren().add(target);




        //Use arrayList.... reference second hwk


        layout.setRight(sideM);

        //layout.getRight().setLayoutY(50);
        //layout.getRight().setLayoutY(50);
    }

    public Label create(String literal){
        Label title4= new Label(literal);
        title4.setStyle("-fx-text-fill:black");
        title4.setFont(Font.font(20));

        return title4;
    }

    public void initStyleG2(){
        initStyleG(layout);
    }

    public void initStyleG(BorderPane layout){

        centergrid= new VBox(5);
        diagram= new GridPane();
        diagram.setHgap(20);
        diagram.setVgap(20);
        diagram.setPadding(new Insets(150,20,100,110));

        int j=0;
        int i=0;

        while(i<4){

            button1=new Button();
            int r= 150;
            button1.setShape(new Circle(r));
            button1.setMinHeight(70);
            button1.setMinWidth(70);

           // button1.setStyle("-fx-background-color:black");
           // button1.setStyle("-fx-text-fill:white");
            button1.setStyle("-fx-text-fill:white;-fx-background-color:black");

            diagram.add(button1,j,i);

            if(j==3){
                j=0;
                i++;
            }
            else
                j++;
        }
        Button button1= (Button) diagram.getChildren().get(0);
        Button button2= (Button) diagram.getChildren().get(1);
        Button button3= (Button) diagram.getChildren().get(2);
        Button button4= (Button) diagram.getChildren().get(3);
        Button button5= (Button) diagram.getChildren().get(4);
        Button button6= (Button) diagram.getChildren().get(5);
        Button button7= (Button) diagram.getChildren().get(6);
        Button button8= (Button) diagram.getChildren().get(7);
        Button button9= (Button) diagram.getChildren().get(8);
        Button button10= (Button) diagram.getChildren().get(9);
        Button button11= (Button) diagram.getChildren().get(10);
        Button button12= (Button) diagram.getChildren().get(11);
        Button button13= (Button) diagram.getChildren().get(12);
        Button button14= (Button) diagram.getChildren().get(13);
        Button button15= (Button) diagram.getChildren().get(14);
        Button button16= (Button) diagram.getChildren().get(15);

        //button1.setText("B");

        button1.setStyle("-fx-text-fill:white;-fx-background-color:black");
        //button1.setStyle("-fx-background-color:black");

        //button2.setText("U");
        button2.setStyle("-fx-text-fill:white;-fx-background-color:black");


        //button5.setText("Z");
        button5.setStyle("-fx-text-fill:white;-fx-background-color:black");

       // button6.setText("Z");
        button6.setStyle("-fx-text-fill:white;-fx-background-color:black");


        //button11.setText("W");
        button11.setStyle("-fx-text-fill:white;-fx-background-color:black");

       // button12.setText("O");
        button12.setStyle("-fx-text-fill:white;-fx-background-color:black");


        //button15.setText("R");
        button15.setStyle("-fx-text-fill:white;-fx-background-color:black");

        //button16.setText("D");
        button16.setStyle("-fx-text-fill:white;-fx-background-color:black");

        centergrid.getChildren().add(diagram);
        pause= new Button("Pause");
        pause.setMinWidth(100);
        pause.setMinHeight(50);

        HBox new1= new HBox();
        //new1.setPadding(new Insets(0,50,0,0));
        new1.setAlignment(Pos.CENTER);
        new1.getChildren().add(pause);

        centergrid.getChildren().add(new1);


        layout.setCenter(centergrid);

        //LEVEL 1

        File file1= new File("choicesL1.txt");
        try {
            FileReader file2= new FileReader(file1);
            BufferedReader file3= new BufferedReader(file2);

            levelL.setWords(diagram,file3);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

       // levelL.setWords(diagram);



    }

    public void initStyleT2(){
        initStyleT(layout);
    }

    public void initStyleL2(){
        initStyleL(layout);
    }

    public Stage getStage(){
        return primary;
    }


    public Scene getScene(){
        return primaryScene;
    }



    public void initStyleL(BorderPane layout){
        menu=new VBox(25);


        homeB= new Button("Home");
        homeB.setMaxWidth(250);
        homeB.setPadding(new Insets(10,10,10,10));
        /*

        start= new Button("Start Playing");
        start.setMaxWidth(230);
        start.setPadding(new Insets(10,10,10,10));

        //createNewProf.setMaxHeight(100);

        // createNewProf.setStyle();
        login = new Button("Select Mode");
        login.setMaxWidth(230);
        login.setPadding(new Insets(10,10,10,10));
        */

        Label home= new Label("Level Selection");
        home.setFont(Font.font(35));

        home.setStyle("-fx-background-color:white");
        //  home.setStyle("-fx-font:100");

        menu.getChildren().add(home);
        menu.getChildren().add(homeB);
        // menu.getChildren().add(login);
        // menu.getChildren().add(start);


        // layout=new BorderPane();
        layout.setPadding(new Insets(0,100,0,0));
        //layout.getLeft().setLayoutX(50);


        layout.setStyle("-fx-background-color:#DCDCDC");
        layout.setLeft(menu);

        //  layout.getLeft().setLayoutX(50);

        menu.setStyle("-fx-background-color:#000000");

        /*
        login.setOnAction(e-> {
            reset(layout);
            //primary = loginStage;
           // loginscreen.start(loginStage);

            Scene prim2 = loginscreen.getScene();
            // this.primaryScene=loginscreen.getScene();

            primary.close();

            //primary.setScene(prim2);
            //MESSED UP LAYOUT
            //loginStage.setScene(prim2);
            //loginStage.show();


        });
        */
    }

    public void initStyleT(BorderPane layout){

        titleContainer=new VBox();

        titleContainer.setStyle("-fx-background-color:#000000");

        Label title= new Label("     Welcome to BuzzWord");
        title.setFont(Font.font(70));
        title.setTextFill(Color.WHITE);

        title.setMinWidth(1000);

        titleContainer.getChildren().add(title);

        Label title2= new Label("     Famous People");
        title2.setFont(Font.font(50));
        title2.setTextFill(Color.WHITE);

        Label title3= new Label(" Level 1");
        title3.setFont(Font.font(30));
        title3.setTextFill(Color.WHITE);

        titleContainer.setAlignment(Pos.CENTER);
        titleContainer.getChildren().add(title2);
        titleContainer.getChildren().add(title3);

        layout.setTop(titleContainer);
    }

    public void reset(BorderPane layout){

        this.layout.getChildren().removeAll();
        this.layout= new BorderPane();


    }



}
