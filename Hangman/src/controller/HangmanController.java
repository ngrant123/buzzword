package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word

    private Text[]      progressW;
    private Text[]      progressB;
    //private Text[]      container2;

    private HBox wrongGuessBox;
    private HBox goodGuessBox;
    private GridPane rectanglesSpace;
    private ArrayList rec2;
    private Rectangle rec3;
    private BorderPane figurePane;
    private Canvas canvas;
    private GraphicsContext gc;
    private Group root;
    private int hangmanCount;
    private Button button1;
    private int disablingfactor;

    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private Rectangle rec1;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate)
    {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {

        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */


    //EDITED
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        Rectangle rec1= new Rectangle ();
        rec1.setX(50);
        rec1.setY(50);
        rec1.setWidth(60);
        rec1.setHeight(50);

         rec2= new ArrayList<Node>();


        gamedata.init();

        if(root!=null){
           root.getChildren().removeAll();
            hangmanCount=0;
        }

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        wrongGuessBox= gameWorkspace.getWrongBox();
        goodGuessBox= gameWorkspace.getGoodBox();

        rectanglesSpace= gameWorkspace.getRectangleSpace();

        wrongGuessBox.getChildren().addAll(new Label("Wrong Guesses:  "));
        goodGuessBox.getChildren().addAll(new Label("Good Guesses:  "));

        figurePane= gameWorkspace.getFigurePane();
        instantiateFigurePane(figurePane);


        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters, rectanglesSpace,rec1);

        play();
    }

    private void instantiateFigurePane(BorderPane figurePane){
        root= new Group();
        //Group root2= new Group();

        canvas =new Canvas();
        gc= canvas.getGraphicsContext2D();



        Line line1= new Line(0,0,50,0);
        Line line2= new Line (0,0,0,150);

        Line line3= new Line();
        line3.setStartX(0);
        line3.setStartY(150);
        line3.setEndX(100);
        line3.setEndY(150);

        Line line4= new Line();
        line4.setStartX(50);
        line4.setStartY(0);
        line4.setEndX(50);
        line4.setEndY(20);



     //   Line line3= new Line(0,300,0,400);

        root.getChildren().addAll(line2,line1,line3,line4);
        figurePane.setCenter(root);

        button1= new Button("Hint");

        figurePane.setLeft(button1);
         button1.setDisable(true);
    }



    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success)
                //endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());
                        endMessage= endMessage;


            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }


    //EDITED
    private void initWordGraphics(HBox guessedLetters,GridPane rectanglesSpace,Rectangle rec1) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        int x=30;
        int y=30;
        int setX= 40;
        int setY=30;
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);


        for(int i=0;i<progress.length;i++){
            rec3= new Rectangle ();
            rec3.setX(x);
            rec3.setY(y);
            rec3.setWidth(setX);
            rec3.setHeight(setY);

            rec2.add(rec3);

            //rectanglesSpace.getChildren().add(rec1);
        }

         for(int j=0;j<rec2.size();j++) {

             rectanglesSpace.add((Node) rec2.get(j),j,0);
             //rectanglesSpace.getChildren().addAll(rec2);
         }
         //figurePane
    }

    public void play() throws IllegalArgumentException{
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
               // final int[] j = {0};
               // String targetTest= gamedata.getTargetWord();

                char[] targetWord= gamedata.getTargetWord().toCharArray();
                 disablingfactor=0;

                progressB= new Text[26];
                progressW= new Text[26];
               // container2=new Text[26];

                int test1= gamedata.getRemainingGuesses();
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                if(gamedata.getTargetWord().length()>7) {

                    // for (int i = 0; i < progress.length; i++){
                    int size = gamedata.getTargetWord().length() - 1;
                    button1.setDisable(false);

                    //  int randomChar = (int) (Math.random() * ((size - 0) + 1));

                    button1.setOnAction(e -> {
                        boolean decision = true;


                        while (decision != false) {


                            int randomChar = (int) (Math.random() * ((size - 0) + 1));

                            if (!alreadyGuessed(gamedata.getTargetWord().charAt(randomChar))) {
                                for (int j = 0; j < progress.length; j++) {
                                    //  int randomChar = (int) (Math.random() * ((9 - 0) + 1));
                                    //System.out.println(gamedata.getTargetWord().charAt(j));

                                    if (gamedata.getTargetWord().charAt(j) == gamedata.getTargetWord().charAt(randomChar)) {
                                        displayCharacter(gamedata.getTargetWord().charAt(randomChar), j);
                                        decision = false;
                                    }
                                }
                            }
                        }
                        //setDisablingfactor(1)
                        figurePane.getChildren().removeAll(button1);

                    });

                }
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {


                    System.out.println(gamedata.getTargetWord());
                    char guess = event.getCharacter().charAt(0);

                    String targetTest= gamedata.getTargetWord();

                    if(guess <'a' || guess >'z' ){

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Information Dialog");
                        alert.setHeaderText("Information Dialog");
                        alert.setContentText("That is not a character");

                        alert.showAndWait();


                    }
                    else if (!alreadyGuessed(guess)) {
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {

                                for (int k = 0; k < 26; k++) {

                                    if (progressW[k] == null) {
                                        progressW[k] = new Text(Character.toString(guess));
                                        goodGuessBox.getChildren().addAll(progressW[k]);
                                        break;
                                    }
                                    break;

                                }
                                break;

                            }
                        }
                        boolean goodguess = false;
                       for (int i = 0; i < progress.length; i++) {
                           //  int i=0;
                           if (gamedata.getTargetWord().charAt(i) == guess) {

                                goodguess=true;
                               displayCharacter(guess, i);
                           }
                       }
                        if (!goodguess){
                                hangmanCount++;

                                gamedata.addBadGuess(guess);

                                switch(hangmanCount){

                                    case 1:
                                        //head
                                        Circle circle1= new Circle();
                                        circle1.setRadius(10);
                                        circle1.setCenterX(50);
                                        circle1.setCenterY(30);

                                        root.getChildren().add(circle1);
                                        break;

                                    case 2:
                                        //spine
                                        Line line5 =new Line();

                                        line5.setStartX(50);
                                        line5.setStartY(40);
                                        line5.setEndX(50);
                                        line5.setEndY(70);

                                        root.getChildren().add(line5);
                                        break;

                                    case 3:
                                        //left
                                        Line line6= new Line();

                                        line6.setStartX(50);
                                        line6.setStartY(55);
                                        line6.setEndX(40);
                                        line6.setEndY(45);

                                        root.getChildren().add(line6);
                                        break;

                                    case 4:
                                        //left part
                                        Circle circle2= new Circle();

                                        circle2.setRadius(5);
                                        circle2.setCenterX(35);
                                        circle2.setCenterY(40);

                                        root.getChildren().add(circle2);
                                        break;


                                    case 5:
                                        //right
                                        Line line7= new Line();

                                        line7.setStartX(50);
                                        line7.setStartY(55);
                                        line7.setEndX(60);
                                        line7.setEndY(45);

                                        root.getChildren().add(line7);
                                        break;

                                    case 6:
                                        //right part
                                        Circle circle3= new Circle();

                                        circle3.setRadius(5);
                                        circle3.setCenterX(65);
                                        circle3.setCenterY(40);

                                        root.getChildren().add(circle3);
                                        break;


                                    case 7:
                                        //left leg
                                        Line line8= new Line();

                                        line8.setStartX(50);
                                        line8.setStartY(70);
                                        line8.setEndX(40);
                                        line8.setEndY(85);

                                        root.getChildren().add(line8);

                                        break;


                                    case 8:
                                        //left leg
                                        Circle circle4=  new Circle();

                                        circle4.setRadius(5);
                                        circle4.setCenterX(35);
                                        circle4.setCenterY(90);

                                        root.getChildren().add(circle4);

                                        break;

                                    case 9:
                                        //right leg
                                        Line line9= new Line();

                                        line9.setStartX(50);
                                        line9.setStartY(70);
                                        line9.setEndX(60);
                                        line9.setEndY(85);

                                        root.getChildren().add(line9);

                                        break;

                                    case 10:
                                        //right leg
                                        Circle circle5=  new Circle();

                                        circle5.setRadius(5);
                                        circle5.setCenterX(65);
                                        circle5.setCenterY(90);

                                        root.getChildren().add(circle5);

                                        break;

                                }

                        }
                        if(!goodguess) {

                            for (int k = 0; k < 26; k++) {

                                if (progressB[k] == null) {
                                    progressB[k] = new Text(Character.toString(guess));
                                    wrongGuessBox.getChildren().addAll(progressB[k]);
                                    break;
                                }
                                break;
                            }
                        }


                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }

                    setGameState(GameState.INITIALIZED_MODIFIED);
                });
                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    stop();
                }

            }

            @Override
            public void stop() {
                displayALL();
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void displayALL() {


        for (int i = 0; i < gamedata.getTargetWord().length(); i++) {
            Character guess = gamedata.getTargetWord().charAt(i);
            //  if (!alreadyGuessed(guess)) {
            displayCharacter(guess, i);
            //}

        }

    }

    private void setDisablingfactor(int i){
        this.disablingfactor=i;
    }

    private int getDisablingfactor(){
        return this.disablingfactor;
    }


    private void displayCharacter(Character guess, int i){

        // progress[i].setVisible(true);
        gamedata.addGoodGuess(guess);
        //  goodguess = true;
        discovered++;

        // Rectangle test= (Rectangle) rec2.get(0);

        rec2.remove(i);

        //container2[0]=Character.toString(guess);
        int sizee= rec2.size();
        int sixe= rectanglesSpace.getChildren().size();

        rec2.add(i, guess);


        int m=sixe-1;
        for (int j = rec2.size()-1; j >=0; j--) {
            // int sixe= rectanglesSpace.getChildren().size();
            //  if(m>=0) {
            rectanglesSpace.getChildren().remove(j);
            sixe = rectanglesSpace.getChildren().size();
            //     m--;
            //  }
        }
        sixe=rectanglesSpace.getChildren().size();

        for (int r = 0; r < rec2.size(); r++) {
            String test4= rec2.get(r).toString();
            if(test4.charAt(0)=='R'){
                // Rectangle test2= (Rectangle) rec2.get(r);
                rectanglesSpace.add((Node) rec2.get(r), r, 0);
            }

            else if(test4.charAt(0)==guess) {
                Text []container2=new Text[26];
                container2[0]= new Text(Character.toString(guess));
                rectanglesSpace.add((container2[0]), r, 0);
            }

            else {
                Text []container3=new Text[26];
                container3[0]= new Text(Character.toString(test4.charAt(0)));
                rectanglesSpace.add(container3[0], r, 0);
            }
        }
    }

    private void displayCharacter2(Character guess, int i){

        // progress[i].setVisible(true);
        gamedata.addGoodGuess(guess);
        //  goodguess = true;
        discovered++;

        // Rectangle test= (Rectangle) rec2.get(0);

        rec2.remove(i);

        //container2[0]=Character.toString(guess);
        int sizee= rec2.size();
        int sixe= rectanglesSpace.getChildren().size();

        rec2.add(i, guess);


        int m=sixe-1;
        for (int j = rec2.size()-1; j >=0; j--) {
            // int sixe= rectanglesSpace.getChildren().size();
            //  if(m>=0) {
            rectanglesSpace.getChildren().remove(j);
            sixe = rectanglesSpace.getChildren().size();
            //     m--;
            //  }
        }
        sixe=rectanglesSpace.getChildren().size();

        for (int r = 0; r < rec2.size(); r++) {
            String test4= rec2.get(r).toString();
            if(test4.charAt(0)=='R'){
                // Rectangle test2= (Rectangle) rec2.get(r);
                rectanglesSpace.add((Node) rec2.get(r), r, 0);
            }

            else if(test4.charAt(0)==guess) {
                Text []container2=new Text[26];
                container2[0]= new Text(Character.toString(guess));
                rectanglesSpace.add((container2[0]), r, 0);
            }

            else {
                Text []container3=new Text[26];
                container3[0]= new Text(Character.toString(test4.charAt(0)));
                rectanglesSpace.add(container3[0], r, 0);
            }
        }
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        restoreWordGraphics(guessedLetters);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;
        }
        guessedLetters.getChildren().addAll(progress);
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
